package com.example.laluisangelv1.Recorridos.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.R


class SeccionesEstadisticasListAdapter internal constructor(context: Context
) : RecyclerView.Adapter<SeccionesEstadisticasListAdapter.SeccionViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var secciones = emptyList<String>() // Cached copy of words

    inner class SeccionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val sitioItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeccionViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return SeccionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SeccionViewHolder, position: Int) {
        val current = secciones[position]
        holder.sitioItemView.text = current
    }

    internal fun setSecciones(secciones: List<String>) {
        this.secciones = secciones
        Log.i("EstadisticasAdapter", secciones.size.toString())
        notifyDataSetChanged()
    }

    override fun getItemCount() = secciones.size
}