package com.example.laluisangelv1.Secciones

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.laluisangelv1.Parqueadero.ParqueaderoData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * This is the backend. The database. This used to be done by the OpenHelper.
 * The fact that this has very few comments emphasizes its coolness.
 */
@Database(entities = [Recorrido::class, Seccion::class,Parqueaderos::class,Comentarios::class], version = 2, exportSchema = true)
abstract class RecorridoRoomDatabase : RoomDatabase() {

    abstract fun recorridoDao(): RecorridoDao
    companion object {
        @Volatile
        private var INSTANCE: RecorridoRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): RecorridoRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RecorridoRoomDatabase::class.java,
                    "recorridos_database"
                )
                    // Wipes and rebuilds instead of migrating if no Migration object.
                    // Migration is not part of this codelab.
                    .fallbackToDestructiveMigration()
                    .addCallback(RecorridoDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class RecorridoDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            /**
             * Override the onOpen method to populate the database.
             * For this sample, we clear the database every time it is created or opened.
             */
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                // If you want to keep the data through app restarts,
                // comment out the following line.
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.recorridoDao())
                    }
                }
            }
        }

        /**
         * Populate the database in a new coroutine.
         * If you want to start with more words, just add them.
         */
        fun populateDatabase(recorridoDao: RecorridoDao) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.

            var recorrido = Recorrido(0,"01-01-2020","01-01-2020",16)
            recorridoDao.insertRecorrido(recorrido)

            var seccion = Seccion("Cafeteria",0,"Si quieres refrescarte y comer algo antes o despues de tu recorrido por la Biblioteca. Puedes pasar al lado de la Salida para probar unos ricos y deliciosos snacks! ",5)
            recorridoDao.insertSeccion(seccion)

        }
    }

}