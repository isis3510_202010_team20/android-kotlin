package com.example.laluisangelv1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.Home.HomeActivity
import com.example.laluisangelv1.Recorridos.ModoRecorridoActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login2.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]

    @BindView(R.id.correoLoginText)
    @JvmField
    var emailView: EditText? = null

    @BindView(R.id.contraseñaLoginText)
    @JvmField
    var passwordView: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login2)

        //ButterKnife
        ButterKnife.bind(this)

        // Buttons
        loginButton.setOnClickListener(this)

        // [START initialize_auth]
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        // [END initialize_auth]

        verificarCurrentUser()
    }

    private fun signIn() {

        Log.d("LoginActivity", "signIn:$emailView.text.toString().trim()")
        if (!validateForm()) {
            return
        }

        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(emailView!!.text.toString().trim(), passwordView!!.text.toString().trim())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("LoginActivity", "signInWithEmail:success")
                    val user = auth.currentUser

                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish();
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("LoginActivity", "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

            }
        // [END sign_in_with_email]
    }

    override fun onClick(v: View) {
        val i = v.id
        when (i) {
            R.id.loginButton -> signIn()

        }
    }

    fun goSignUpActivity(view: View) {

        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    private fun verificarCurrentUser ()
    {
        if (auth.currentUser != null)
        {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }



    private fun validateForm(): Boolean {
        var valid = true

        if (TextUtils.isEmpty(emailView!!.text)) {
            emailView!!.error = "Required."
            valid = false
        } else {
            emailView!!.error = null
        }

        val password = passwordView!!.text.toString()
        if (TextUtils.isEmpty(passwordView!!.text)) {
            passwordView!!.error = "Required."
            valid = false
        } else {
            passwordView!!.error = null
        }

        return valid
    }



}
