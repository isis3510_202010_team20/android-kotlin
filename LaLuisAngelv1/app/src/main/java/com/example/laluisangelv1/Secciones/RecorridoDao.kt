package com.example.laluisangelv1.Secciones

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.laluisangelv1.Parqueadero.Parqueadero
import com.example.laluisangelv1.Parqueadero.ParqueaderoData

@Dao
interface RecorridoDao {

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data. Notifies its active observers when the
    // data has changed. Since we are getting all the contents of the database,
    // we are notified whenever any of the database contents have changed.
    @Query("SELECT * from seccion_table")
    fun getRecorridoSecciones(): LiveData<List<Seccion>>

    @Query("SELECT * from parqueaderos")
    fun getParqueaderos(): LiveData<List<Parqueaderos>>

    @Query("SELECT * from comentarios")
    fun getComentarios(): LiveData<List<Comentarios>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertRecorrido(recorrido: Recorrido)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSeccion(seccion: Seccion)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertParqueadero(parqueadero: Parqueaderos)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertComentario(comentarios: Comentarios)

    @Query("DELETE FROM recorrido_table")
    fun deleteAllFromRecorridos()

    @Query("DELETE FROM seccion_table")
    fun deleteAllFromSecciones()
}
