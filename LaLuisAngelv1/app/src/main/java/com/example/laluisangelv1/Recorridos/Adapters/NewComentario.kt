package com.example.laluisangelv1.Recorridos.Adapters

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.NumberPicker
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.SeccionActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_new_seccion.*


class NewComentario : AppCompatActivity(), View.OnClickListener  {
    lateinit var db: FirebaseFirestore
    private var seccionActual = ""
    var idRecorrido: Int = 0
    var duracionAcumulada: Int = 0
    var userName: String  = ""

    //ButterKnife
    @BindView(R.id.valoracion)
    @JvmField
    var valoracion:NumberPicker? = null

    @BindView(R.id.username)
    @JvmField
    var user: EditText? = null

    @BindView(R.id.comentario)
    @JvmField
    var comentario: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_seccion)

        //ButterKnife
        ButterKnife.bind(this)

        valoracion!!.maxValue = 5 //set maximum val
        valoracion!!.minValue = 1 //set minimum val
        val b = intent.extras
        if (b != null)
        {
            seccionActual = b.getString("seccionActual").toString()
            idRecorrido = b.getInt("seccionActualidRecorrido")
            duracionAcumulada = b.getInt("duracionAcumulada")
            idRecorrido = b.getInt("idRecorrido")
        }
        button_save.setOnClickListener(this)
    }
    override fun onClick(v: View) {

        val i = v.id
        when (i) {
            R.id.button_save -> publicar()
        }

    }
    private fun publicar()
    {
        db = FirebaseFirestore.getInstance()
        var lista:MutableList<String> = mutableListOf<String>()
        Log.d("PruebaA1.1", valoracion!!.value.toString())
        lista.add(user!!.text.toString())
        lista.add(comentario!!.text.toString())
        lista.add(valoracion!!.value.toString())
        db.collection("secciones").document("$seccionActual")
            .collection("Comentarios").document("data").get()
            .addOnSuccessListener { document ->
                db.collection("secciones").document("$seccionActual")
                    .collection("Comentarios").document("data").update(mapOf(
                        "cmnt"+(document.data?.size!!+1) to lista
                    ))


            }
            .addOnFailureListener { exception ->
                Log.d("InstruccionesActivity", "get failed with ", exception)
            }

        val intent = Intent(this,SeccionActivity::class.java)
        intent.putExtra("seccionActual",seccionActual)
        intent.putExtra("seccionActualidRecorrido",idRecorrido )
        intent.putExtra("duracionAcumulada",duracionAcumulada )
        intent.putExtra("idRecorrido",idRecorrido )
        startActivity(intent)
        finish();

    }

}
