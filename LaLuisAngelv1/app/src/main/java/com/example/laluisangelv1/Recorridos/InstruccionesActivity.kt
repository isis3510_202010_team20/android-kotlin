package com.example.laluisangelv1.Recorridos

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.Adapters.InstruccionListAdapter
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class InstruccionesActivity : AppCompatActivity() {

    lateinit var db: FirebaseFirestore
    lateinit var query: Query
    var seccionActualName: String = "Entrada"
    var seccionTargetString: String = "Punto de Informacion"

    @BindView(R.id.seccionTargetView)
    @JvmField
    var seccionTargetName: TextView? = null

    @BindView(R.id.instrucciones_rv)
    @JvmField
    var recyclerView: RecyclerView? = null

    private var duracionAcumulada: Int = 0
    private var listaInstrucciones: MutableList<String> = mutableListOf<String>()
    private var idRecorrido: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instrucciones)

        //ButterKnife
        ButterKnife.bind(this)

        val extras = intent.extras


        if (extras != null) {
            seccionActualName= extras.getString("seccionActual").toString()
            seccionTargetName!!.text = extras.getString("seccionTarget").toString()
            seccionTargetString = extras.getString("seccionTarget").toString()
            duracionAcumulada= extras.getInt("duracionAcumulada")
            idRecorrido = extras.getInt("idRecorrido")

            seccionTargetName!!.text = "En camino a ${seccionTargetName!!.text}"
        }

        CoroutineScope(IO).launch{
            getListaInstrucciones()
        }

    }

    private suspend fun getListaInstrucciones()
    {
        val adapter = InstruccionListAdapter(this)

        db = FirebaseFirestore.getInstance()

        Log.i("seccionInicioName","$seccionActualName")
        Log.i("seccionTargetName","$seccionTargetName")

        // [START get_multiple_all]
        var docRef = db.collection("secciones").document("${seccionActualName.toString()}")
            .collection("seccionesCercanas").document("$seccionTargetString")

        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.i("InstruccionesActivitySA",seccionActualName)

                    Log.i("InstruccionesActivity",document.get("instrucciones").toString())
                    listaInstrucciones = document.get("instrucciones") as MutableList<String>
                    adapter.setInstrucciones(listaInstrucciones.toList())
                    adapter.setSeccionTarget(seccionTargetString.toString())
                    adapter.setDuracionAcumulada(duracionAcumulada)
                } else {
                    Log.d("InstruccionesActivity", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("InstruccionesActivity", "get failed with ", exception)
            }

        adapter.setRecorridoId(this.idRecorrido)
        setListaInstrucciones(adapter,this)
    }

    private suspend fun setListaInstrucciones(adapter: InstruccionListAdapter, context: Context)
    {
        withContext(Dispatchers.Main){
            recyclerView!!.adapter = adapter
            recyclerView!!.layoutManager = LinearLayoutManager(context)
        }
    }



}
