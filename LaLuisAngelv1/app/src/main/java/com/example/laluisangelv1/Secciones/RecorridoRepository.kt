package com.example.laluisangelv1.Secciones

import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.laluisangelv1.Parqueadero.Parqueadero
import com.example.laluisangelv1.Parqueadero.ParqueaderoData

class RecorridoRepository(private val recorridoDao: RecorridoDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val totalSecciones: LiveData<List<Seccion>> = recorridoDao.getRecorridoSecciones()
    val totalParqueaderoReservado: LiveData<List<Parqueaderos>> = recorridoDao.getParqueaderos()
    val totalComentarios: LiveData<List<Comentarios>> = recorridoDao.getComentarios()
    // You must call this on a non-UI thread or your app will crash. So we're making this a
    // suspend function so the caller methods know this.
    // Like this, Room ensures that you're not doing any long running operations on the main
    // thread, blocking the UI.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertRecorrido(recorrido: Recorrido) {
        recorridoDao.insertRecorrido(recorrido)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertSeccion(seccion: Seccion) {
        recorridoDao.insertSeccion(seccion)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertParqueadero(parqueadero: Parqueaderos) {
        recorridoDao.insertParqueadero(parqueadero)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertComentario(comentario: Comentarios) {
        recorridoDao.insertComentario(comentario)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAll() {
        recorridoDao.deleteAllFromRecorridos()
        recorridoDao.deleteAllFromSecciones()
    }
}