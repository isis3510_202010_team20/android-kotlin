package com.example.laluisangelv1.Recorridos.Adapters

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.InstruccionesActivity
import com.example.laluisangelv1.Recorridos.ModoRecorridoActivity
import com.example.laluisangelv1.Recorridos.SeccionActivity
import com.example.laluisangelv1.Secciones.Seccion

class SeccionListAdapter internal constructor(activity: ModoRecorridoActivity) : RecyclerView.Adapter<SeccionListAdapter.SeccionViewHolder>()

{

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var secciones = emptyList<String>() // Cached copy of words
    private var seccionActual = "Entrada" // Sitio Actual
    private var duracionAcumulada: Int = 0
    private var idRecorrido: Int = 0
    private var conected:Boolean = true;

    private var intentInstruccionesActivity = Intent(activity, InstruccionesActivity::class.java)
    private var bundleExtrasIntent: Bundle = Bundle()
    private val contextAdapter = activity

    inner class SeccionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val sitioItemView: Button = itemView.findViewById(R.id.seccionCercanaButtonItem)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeccionViewHolder {
        val itemView = inflater.inflate(R.layout.seccion_cercana_rv_item, parent, false)

        return SeccionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SeccionViewHolder, position: Int) {
        val target = secciones[position]
        Log.i("SeccionListAdapterA",getSeccionActual())
        Log.i("SeccionListAdapter",target)
        holder.sitioItemView.text = target
        holder.sitioItemView.setOnClickListener {

            bundleExtrasIntent.putInt("duracionAcumulada",duracionAcumulada)
            bundleExtrasIntent.putInt("idRecorrido",idRecorrido)

            if(conected)
            {

                bundleExtrasIntent.putString("seccionActual",getSeccionActual())
                bundleExtrasIntent.putString("seccionTarget",target)

                intentInstruccionesActivity.putExtras(bundleExtrasIntent)
                contextAdapter.startActivity(intentInstruccionesActivity)
            }
            else{
                bundleExtrasIntent.putString("seccionActual",target)
                val intent = Intent(contextAdapter, SeccionActivity::class.java)

                intent.putExtras(bundleExtrasIntent)
                contextAdapter.startActivity(intent)
             }
        }
    }
    internal fun setIdRecorrido(idRecorrido: Int){
        this.idRecorrido = idRecorrido
    }
    internal fun setConectividad(conectado: Boolean){
        this.conected = conectado
    }
    internal fun setSecciones(secciones: List<String>) {
        this.secciones = secciones
        notifyDataSetChanged()
    }
    internal fun setSeccionesClass(secciones: List<Seccion>) {
        var myList: MutableList<String> = mutableListOf<String>()
        secciones.forEach {
            myList.add(it.nombreseccion)
        }
        this.secciones = myList
        notifyDataSetChanged()
    }
    internal fun setSeccionActual(seccion: String) {
        this.seccionActual = seccion
        notifyDataSetChanged()
    }

    internal fun getSeccionActual(): String {
        return this.seccionActual;
    }

    internal fun setDuracionAcumulada(duracionAcum: Int) {
        this.duracionAcumulada = duracionAcum
    }

    override fun getItemCount() = secciones.size
}
