package com.example.laluisangelv1.Recorridos

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ButtonBarLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.Adapters.ComentariosListAdapter
import com.example.laluisangelv1.Recorridos.Adapters.NewComentario
import com.example.laluisangelv1.Recorridos.Model.Firebase.FirebaseRepository
import com.example.laluisangelv1.Recorridos.Model.PorcentajeViewModel
import com.example.laluisangelv1.Secciones.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_seccion.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class SeccionActivity : AppCompatActivity(), View.OnClickListener {

    // Seccion Instances
    // KnifeButtler

    @BindView(R.id.nombreSeccion)
    @JvmField
    var seccionActualTextView: TextView? = null

    @BindView(R.id.descripcionSeccion)
    @JvmField
    var descripcionSeccionText: TextView? = null

    @BindView(R.id.comentarios_rv)
    @JvmField
    var rvComentarios: RecyclerView? = null

    @BindView(R.id.cancelarModoRecorridoButton)
    @JvmField
    var btnEnd: FloatingActionButton? = null

    @BindView(R.id.modoRecorridoButton_ins)
    @JvmField
    var btnRecorrido: FloatingActionButton? = null

    @BindView(R.id.floatingActionButtonComentarios)
    @JvmField
    var btnComentarios: FloatingActionButton? = null

    //// Boolean que indica si el dispositivo esta conectado o no
    var isConnected: Boolean = true

    public var seccionActual: String = "nombre_seccion"
    var tiempoInicio: Date = Date()
    var tiempoFinal: Date = Date()
    var idRecorrido: Int = 0
    var duracionAcumulada: Int = 0
    var userName: String  = ""

    private val minutosenMillis: Int = 60000
    private val segundosenMillis: Int = 1000
    private var listaTot: MutableList<String> = mutableListOf<String>()

    var result:MutableList<String> = mutableListOf<String>()
    private lateinit var recorridoViewModel: RecorridoViewModel

    // Firebase Instances
    private lateinit var auth: FirebaseAuth
    lateinit var db: FirebaseFirestore
    private lateinit var fb_repository: FirebaseRepository
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seccion)

        // ButterKnife
        ButterKnife.bind(this)

        //INIT Tiempo de Inicio de la Actividad
            initTiempoInicial()

        //Get Intent Extras and Update Seccion Actual Text View
            getIntentExtras()
        // Init Button Listeners
            initButtonListeners()

        // Init Recorrido View Model
            initRecorridoViewModel()

        // Init Firebase Instances


            initFirebaseInstances()




            comentariosSeccion()


    }

    public override fun onNewIntent(intent: Intent) {
        Log.i("SeccionActivity", "onNewIntent()")
        super.onNewIntent(intent)
        updateSeccionUI(intent)
    }

    public override fun onRestart() {
        super.onRestart()
        Log.i("SeccionActivity", "onRestart()")
        // Check if user is signed in (non-null) and update UI accordingly.
    }

    public override fun onStart() {
        super.onStart()
        Log.i("SeccionActivity", "onStart()")
        // Check if user is signed in (non-null) and update UI accordingly.
    }

    public override fun onResume() {
        super.onResume()
        Log.i("SeccionActivity", "onResume()")
       // updateSeccionUI()
    }

    public override fun onPause()
    {
        super.onPause()
        Log.i("SeccionActivity", "onPause()")
    }

    public override fun onStop() {
        super.onStop()
        Log.i("SeccionActivity", "onStop()")
    }

    public override fun onDestroy()
    {
        super.onDestroy()
        Log.i("SeccionActivity", "onDestroy()")
    }

    private fun initTiempoInicial()
    {
        tiempoInicio = Calendar.getInstance().time
        Log.i("TiempoInicioSeccion",tiempoInicio.toString())

        rvComentarios!!.addItemDecoration(DividerItemDecoration(rvComentarios!!.getContext(), DividerItemDecoration.VERTICAL))
    }

    private fun getIntentExtras()
    {
        val b = intent.extras
        if (b != null)
        {
            seccionActual = b.getString("seccionActual").toString()
            duracionAcumulada = b.getInt("duracionAcumulada")
            idRecorrido = b.getInt("idRecorrido")
        }

        seccionActualTextView!!.text = seccionActual
    }

    private fun initButtonListeners()
    {
        modoRecorridoButton_ins.setOnClickListener(this)
        cancelarModoRecorridoButton.setOnClickListener(this)
        floatingActionButtonComentarios.setOnClickListener(this)
    }

    private fun initRecorridoViewModel()
    {
        val a =this

            val adapter = RecorridoListAdapter(a)
            recorridoViewModel = ViewModelProvider(a).get(RecorridoViewModel::class.java)

            recorridoViewModel.totalSecciones.observe(a, Observer { secciones ->
                // Update the cached copy of the words in the adapter.
                secciones?.let { adapter.setSecciones(secciones) }
            })


    }

    private fun initFirebaseInstances()
    {

        // Verifica la conexión del dispositivo
        checkDeviceConnection()


        if(isConnected) {
            val a  = this

            fb_repository = FirebaseRepository(a)
            db = FirebaseFirestore.getInstance()
            auth = FirebaseAuth.getInstance()
            // [START shared_app_measurement]
            // Obtain the FirebaseAnalytics instance.
            firebaseAnalytics = FirebaseAnalytics.getInstance(a)
            // [END shared_app_measurement]

            if (auth != null) {
                firebaseAnalytics.setUserId(auth.currentUser?.email);
                userName = auth.currentUser?.email.toString()
            }

            else
            {
                firebaseAnalytics.setUserId("0");
            }

            btnEnd!!.setVisibility(View.VISIBLE);
            btnRecorrido!!.setVisibility(View.VISIBLE);

            db = FirebaseFirestore.getInstance()

            getDescripcionTextView()
        }
        else{
            btnComentarios!!.setVisibility(View.GONE);
            btnEnd!!.setVisibility(View.GONE);
            btnRecorrido!!.setVisibility(View.GONE);
        }
    }

    override fun onClick(v: View)
    {

        val i = v.id
        when (i) {
            R.id.modoRecorridoButton_ins -> goModoRecorridoActivity(v)
            R.id.cancelarModoRecorridoButton -> cancelarModoRecorrido()
            R.id.floatingActionButtonComentarios -> goNuevoComentario()
        }

    }

    private fun goNuevoComentario()
    {
        val intent = Intent(this,NewComentario::class.java)
        intent.putExtra("seccionActual",seccionActual)
        intent.putExtra("seccionActualidRecorrido",idRecorrido )
        intent.putExtra("duracionAcumulada",duracionAcumulada )
        intent.putExtra("idRecorrido",idRecorrido )

        startActivity(intent)

    }

    private fun goModoRecorridoActivity(v:View)
    {

        // Obtiene el tiempo en mins que el usuario permacio en la seccion
        tiempoFinal = Calendar.getInstance().time

        Log.i("TiempoFinalSeccion",tiempoFinal.time.toString())

        val diferencia = tiempoFinal.time - tiempoInicio.time
        val duracionSeccionMillis = tiempoFinal.time - tiempoInicio.time
        val nuevaDuracionAcumulada = duracionAcumulada + duracionSeccionMillis

        fb_repository.actualizarDatosSeccion(auth.currentUser?.email.toString(),seccionActual,diferencia)

        val seccion = Seccion(seccionActual,idRecorrido,"descripcionSeccion",diferencia.toInt())
        recorridoViewModel.insertSeccion(seccion)

        // [START custom_event]
        val params = Bundle()
        params.putString("nombreUsuario", userName)
        params.putString("seccion", seccionActual)
        params.putInt("duracionAcumuladaMillis",nuevaDuracionAcumulada.toInt())
        firebaseAnalytics.logEvent("visitaSeccion", params)
        // [END custom_event]

        Log.i("DuracionAcumulada",diferencia.toString())

        //Pasar a Modo Recorrido
        val intent = Intent(this,ModoRecorridoActivity::class.java)
        intent.putExtra("seccionActual",seccionActual)
        intent.putExtra("duracionAcumulada",nuevaDuracionAcumulada.toInt())
        intent.putExtra("idRecorrido",idRecorrido)
        startActivity(intent)
    }

    private fun cancelarModoRecorrido()
    {
        val builder = AlertDialog.Builder(this)

        // Set the alert dialog title
        builder.setTitle("Salir del Modo Recorrido")

        // Display a message on alert dialog
        builder.setMessage("¿Estás seguro de que deseas salir del Modo Recorrido?")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("SÍ"){dialog, which ->
            // Do something when user press the positive button

            // Obtiene el tiempo en mins que el usuario permacio en la seccion
            tiempoFinal = Calendar.getInstance().time

            Log.i("TiempoFinalSeccion",tiempoFinal.time.toString())

            val duracionSeccionMillis = tiempoFinal.time - tiempoInicio.time
            val nuevaDuracionAcumulada = duracionAcumulada + duracionSeccionMillis

            fb_repository.actualizarDatosSeccion(auth.currentUser?.email.toString(),seccionActual,duracionSeccionMillis)

            val seccion = Seccion(seccionActual,idRecorrido,"descripcionSeccion",duracionSeccionMillis.toInt())
            recorridoViewModel.insertSeccion(seccion)

            //Guardar En local
            val recorrido = Recorrido(idRecorrido,"01-01-2020" ,"01-01-2020",nuevaDuracionAcumulada.toInt())
            recorridoViewModel.insertRecorrido(recorrido)

            // [START custom_event]
            val params = Bundle()
            params.putString("nombreUsuario", userName)
            params.putString("seccion", seccionActual)
            params.putInt("duracionAcumuladaMillis",nuevaDuracionAcumulada.toInt())
            firebaseAnalytics.logEvent("visitaSeccion", params)
            // [END custom_event]

            Log.i("DuracionAcumulada",nuevaDuracionAcumulada.toString())

            val intent = Intent(this,CongratsActivity::class.java)
            intent.putExtra("seccionActual",seccionActual)
            intent.putExtra("duracionAcumulada",nuevaDuracionAcumulada.toInt())
            intent.putExtra("idRecorrido",idRecorrido)

            startActivity(intent)

        }

        // Display a negative button on alert dialog
        builder.setNegativeButton("Cancelar"){dialog,which ->

        }


        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }

    private fun comentariosSeccion()
    {

        checkDeviceConnection()

        getComentariosSeccion()

    }

    private fun updateSeccionUI(intent: Intent)
    {
        // Se reestablece el tiempo inicial
        tiempoInicio = Calendar.getInstance().time
        Log.i("TiempoInicioSeccion",tiempoInicio.toString())

        // Se obtienen los valores del nuevo Intent
        val b = intent.extras
        if (b != null)
        {

            seccionActual = b.getString("seccionActual").toString()
            duracionAcumulada = b.getInt("duracionAcumulada")
            idRecorrido = b.getInt("idRecorrido")
        }
        seccionActualTextView!!.text = seccionActual

        // Se actualizan las secciones del RecorridoViewModel
        val adapter = RecorridoListAdapter(this)
        recorridoViewModel = ViewModelProvider(this).get(RecorridoViewModel::class.java)
        recorridoViewModel.totalSecciones.observe(this, Observer { secciones ->
            // Update the cached copy of the words in the adapter.
            secciones?.let { adapter.setSecciones(secciones) }
        })

        // Se actualiza el TextView de la descripcion de la sección
        checkDeviceConnection()
        if(isConnected) {

            btnEnd!!.setVisibility(View.VISIBLE);
            btnRecorrido!!.setVisibility(View.VISIBLE);

            getDescripcionTextView()
            getComentariosSeccion()
        }
        else{
            btnComentarios!!.setVisibility(View.GONE);
            btnEnd!!.setVisibility(View.GONE);
            btnRecorrido!!.setVisibility(View.GONE);
        }
    }

    private fun getDescripcionTextView()
    {
        // [START get_multiple_all]
        var docRef = db.collection("secciones").document("${seccionActual.toString()}")

        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    descripcionSeccionText!!.text = document.get("descripcion").toString()

                } else {
                    Log.d("SeccionActivity", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("InstruccionesActivity", "get failed with ", exception)
            }
    }

    private fun getComentariosSeccion()
    {
        val adapter2 = ComentariosListAdapter(this)
        if (isConnected) {
            db.collection("secciones").document("$seccionActual")
                .collection("Comentarios").document("data").addSnapshotListener { document, e ->
                    if (e != null) {
                        Log.w("TAG", "Listen failed.", e)
                        return@addSnapshotListener
                    }
                    if (document != null) {
                        var i = 1
                        while (i < document.data?.size!! + 1) {
                            listaTot = document.data?.get("cmnt" + i) as MutableList<String>
                            //Local
                            var comentarios = Comentarios(listaTot.get(0),listaTot.get(1),listaTot.get(2))
                            recorridoViewModel.insertComentario(comentarios)
                            adapter2.setTotal(listaTot.toList())
                            i++
                        }

                    } else {
                        Log.d("InstruccionesActivity", "No such document")
                    }
                }

            Log.d("SeccionActualPD", seccionActual)
            rvComentarios!!.adapter = adapter2
            rvComentarios!!.layoutManager = LinearLayoutManager(this)

        } else {
            // The onChanged() method fires when the observed data changes and the activity is
            // in the foreground.
            recorridoViewModel.totalComentarios.observe(this, Observer { comentarios ->
                // Update the cached copy of the words in the adapter.
                var myList: MutableList<List<String>> = mutableListOf<List<String>>()
                for(i in 0..(comentarios.size-2))
                {
                    var tempList: MutableList<String> = mutableListOf<String>()
                    tempList.add(comentarios.get(i).user)
                    tempList.add(comentarios.get(i).comentario)
                    tempList.add(comentarios.get(i).valoracion)

                    Log.d("Asdfasdqaedasd", comentarios.get(i).valoracion)
                    myList.add(tempList)
                }

                adapter2.setTotalFull(myList)
            })

            rvComentarios!!.adapter = adapter2
            rvComentarios!!.layoutManager = LinearLayoutManager(this)

        }
    }

    private fun checkDeviceConnection()
    {
        val cm = this@SeccionActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        isConnected = activeNetwork?.isConnectedOrConnecting == true
    }
}
