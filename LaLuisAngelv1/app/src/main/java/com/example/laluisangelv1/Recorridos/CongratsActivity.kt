package com.example.laluisangelv1.Recorridos

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.Home.HomeActivity
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.Model.Firebase.FirebaseRepository
import com.example.laluisangelv1.Secciones.Recorrido
import com.example.laluisangelv1.Secciones.RecorridoViewModel
import com.example.laluisangelv1.Secciones.Seccion
import kotlinx.android.synthetic.main.activity_congrats.*


class CongratsActivity : AppCompatActivity(), View.OnClickListener {


    private lateinit var fb_repository: FirebaseRepository

    var seccionActual: String = "Entrada"
    lateinit var secciones: List<Seccion>
    private lateinit var sitioViewModel: RecorridoViewModel
    var idRecorrido: Int = 0
    private var duracionTotal: Int = 0

    private val minutosenMillis: Int = 60000
    private val segundosenMillis: Int = 1000

    @BindView(R.id.duracionTotalTextView)
    @JvmField
    var duracion: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_congrats)

        //ButterKnife
        ButterKnife.bind(this)


        //Init Firebase Repository
        fb_repository = FirebaseRepository(this)

        val extras = intent.extras

        if (extras != null) {
            seccionActual= extras.getString("seccionActual").toString()
            duracionTotal= extras.getInt("duracionAcumulada")
            idRecorrido = extras.getInt("idRecorrido")
        }
        // Get a new or existing ViewModel from the ViewModelProvider.
        sitioViewModel = ViewModelProvider(this).get(RecorridoViewModel::class.java)

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        sitioViewModel.totalSecciones.observe(this, Observer { sitios ->
            // Update the cached copy of the words in the adapter.
            sitios?.let { this.secciones = it }

        })

        setDuracionTotal()

        OKCongratsButton.setOnClickListener(this)
    }

    private fun setDuracionTotal()
    {
        val elapsedMinutes = duracionTotal/minutosenMillis
        val elapsedSeconds = (duracionTotal)/segundosenMillis

        duracion!!.text = "${elapsedMinutes}m ${elapsedSeconds}s"

    }

    override fun onClick(v: View) {
        val i = v.id
        when (i) {
            R.id.OKCongratsButton -> goHomeActivity(v)
        }
    }

   fun goHomeActivity(view: View)
    {
        val intent = Intent(this,HomeActivity::class.java)
        intent.putExtra("seccionActual",seccionActual)
        intent.putExtra("idRecorrido", idRecorrido)
        startActivity(intent)
    }
}
