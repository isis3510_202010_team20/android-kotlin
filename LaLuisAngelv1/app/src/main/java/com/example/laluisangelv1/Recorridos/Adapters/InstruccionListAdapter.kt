package com.example.laluisangelv1.Recorridos.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.InstruccionesActivity
import com.example.laluisangelv1.Recorridos.ModoRecorridoActivity
import com.example.laluisangelv1.Recorridos.SeccionActivity


class InstruccionListAdapter internal constructor(context: Context) : RecyclerView.Adapter<InstruccionListAdapter.InstruccionViewHolder>()

{

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var instrucciones = emptyList<String>() // Cached copy of Instructions
    private var seccionTarget:String? = ""
    private val contextAdapter = context
    private var actual = 0
    private var duracionAcumulada: Int = 0
    private var idRecorrido: Int = 0

    inner class InstruccionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val instruccionItemView: TextView = itemView.findViewById(R.id.instruccion_TextView)
        val instruccionImagen: ImageView = itemView.findViewById(R.id.instruccion_Imagen)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstruccionViewHolder {
        val itemView = inflater.inflate(R.layout.instruccion_rv_item, parent, false)
        return InstruccionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: InstruccionViewHolder, position: Int) {
        val current = instrucciones[position]
        Log.i("InstruccionListAdapter",current)
        holder.instruccionItemView.text = current
        holder.instruccionItemView.setOnClickListener { view ->
            if(actual == position)
            {

                Toast.makeText(view.context, "Instruccion ${position+1} completada! ", Toast.LENGTH_SHORT)
                    .show()
                holder.instruccionImagen.setImageResource(R.drawable.tick)
                actual ++
                if(actual == instrucciones.size)
                {
                    Log.i("InstruccionLA",seccionTarget)
                    val intent = Intent(contextAdapter, SeccionActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.putExtra("seccionActual",seccionTarget)
                    intent.putExtra("duracionAcumulada",duracionAcumulada)
                    intent.putExtra("idRecorrido",idRecorrido)
                    contextAdapter.startActivity(intent)
                }
            }

        }
    }

    internal fun setRecorridoId(recorridoId: Int){
        this.idRecorrido = recorridoId
        notifyDataSetChanged()
    }

    internal fun setInstrucciones(instrucciones: List<String>) {
        this.instrucciones = instrucciones
        Log.i("SeccionListAdapter",instrucciones.size.toString())
        notifyDataSetChanged()
    }
    internal fun setSeccionTarget(seccionTarget: String?) {
        this.seccionTarget = seccionTarget
        notifyDataSetChanged()
    }
    internal fun setDuracionAcumulada(duracionAcum: Int) {
        this.duracionAcumulada = duracionAcum

    }


    override fun getItemCount() = instrucciones.size

}
