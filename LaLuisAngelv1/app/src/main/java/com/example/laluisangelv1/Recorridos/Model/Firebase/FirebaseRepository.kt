package com.example.laluisangelv1.Recorridos.Model.Firebase

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import androidx.annotation.WorkerThread
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


class FirebaseRepository (context: Context) {

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val sharedPrefFile: String = "com.example.laluisangelv1"
    private val sp = PreferenceManager.getDefaultSharedPreferences(context)
    private val cxt: Context = context

    var porcentajeVisita: Int = 0
    var duracion_total: Int = 0
    var secciones_visitadas: List<String> = emptyList<String>()
    var secciones_no_visitadas: List<String> = emptyList<String>()
    var hashMap_seccionesVisitas: MutableList<String> = mutableListOf<String>()

    companion object {

        val TAG = "FirebaseRepository"
        val USUARIOS = "usuarios"
        val RECORRIDOS = "recorridos"
        val SECCIONES = "secciones"
        val STADISTICS = "stadistics"
        val PORCENTAJE_VISITA = "porcentajeVisita"
        val ULTIMA_SECCION_VISITADA = "ultimaSeccionVisitada"

        val DURACION_TOTAL = "duracionTotal"
        val SECCIONES_VISITADAS = "seccionesVisitadas"
        val SECCIONES_POR_VISITAR = "seccionesPorVisitar"
        val HASHMAP_SECCIONESVISITADAS = "hashMap_seccionesvisitadas"

        val CANTIDAD_SECCIONES = 16
        val ARRAY_NOMBRES_SECCIONES = listOf<String>(
            "Entrada","Cafeteria","Casa Republicana", "Centro de Eventos", "Exposicion Segundo Piso", "Parqueadero",
            "Entrada Parqueadero", "Salida Parqueadero", "Libreria", "Punto de Informacion", "Sala de Conciertos",
            "Sala de Conciertos", "Sala de Lectura General","Sala de Lectura Infantil", "Sala de Lectura Invidentes",
            "Taquilla Sala de Conciertos","Salida")

        private val EXECUTOR = ThreadPoolExecutor(2, 4,
            60, TimeUnit.SECONDS, LinkedBlockingQueue()
        )
    }

    // Crea un usuario con sus respectivas colecciones Recorridos y Stadistics
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun crearUsuario(nombreUsuario: String) {

        var stadisticsArray = arrayListOf<Any>()
        stadisticsArray.addAll(listOf())


        val data = hashMapOf(
            "nombre" to nombreUsuario,
            "recorridos" to arrayListOf<Any>()
        )

        val nestedUltimaSeccionVisitada = hashMapOf(
            "nombreSeccion" to "Entrada",
            "fechaVisita"   to Timestamp(Date())
        )

        val nestedPorcentajeVisita = hashMapOf(
            "valor" to 0,
            "cantidadSeccionesVisitadas" to 0
        )

        val nestedStadistics = hashMapOf(
            PORCENTAJE_VISITA to nestedPorcentajeVisita,
            SECCIONES to arrayListOf<Any>()
        )

        val nestedSecciones = HashMap<String,Any>()

        for (seccion in ARRAY_NOMBRES_SECCIONES)
        {
            val nestedSeccion = hashMapOf(
                "visitada" to false,
                "cantidadVecesVisitada" to 0,
                "duracionAcumuladaMillis" to 0)

            nestedSecciones[seccion] = nestedSeccion
        }

        data[STADISTICS] = nestedStadistics
        data[SECCIONES] = nestedSecciones
        data[ULTIMA_SECCION_VISITADA] = nestedUltimaSeccionVisitada

        db.collection(USUARIOS).document(nombreUsuario).set(data as Map<String, Any>)
            .addOnSuccessListener {
                Log.d(TAG, "Usuario successfully written!")
            }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }
    }

    //Crea un recorrido al momento que el usuario inicia el Modo Recorrido
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun crearRecorrido(nombreUsuario: String, idRecorrido: String) {

        var docRef = db.collection(USUARIOS).document(nombreUsuario)

        val nuevoRecorrido = HashMap<String,Any>()
        nuevoRecorrido[SECCIONES] = arrayListOf<Any>()

        docRef.update(RECORRIDOS, FieldValue.arrayUnion(nuevoRecorrido))
            .addOnSuccessListener {
                Log.d(TAG, "Recorrido successfully written!")
            }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

    }

    //Agrega una sección visitada al Recorrido
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun actualizarDatosSeccion(nombreUsuario: String, nombreSeccion: String, duracionMillis: Long)
    {
        var docRef = db.collection(USUARIOS).document(nombreUsuario)

        docRef.get()
            .addOnSuccessListener {document ->
                if (document != null)
                {
                    //Se actualizan los valores en Secciones y se registra la última visita del usuario
                    var cantidadVisitasSeccion = document.get("secciones.${nombreSeccion}.cantidadVecesVisitada") as Long
                    var duracionAcumulada = document.get("secciones.${nombreSeccion}.duracionAcumuladaMillis") as Long
                    var cantidadSeccionesVisitadas = document.get("stadistics.porcentajeVisita.cantidadSeccionesVisitadas") as Long

                    Log.i(TAG,cantidadSeccionesVisitadas.toString())

                    docRef.update(mapOf(
                        "stadistics.porcentajeVisita.cantidadSeccionesVisitadas" to (cantidadSeccionesVisitadas+1),
                        "secciones.${nombreSeccion}.cantidadVecesVisitada" to (cantidadVisitasSeccion+1),
                        "secciones.${nombreSeccion}.visitada" to true,
                        "secciones.${nombreSeccion}.duracionAcumuladaMillis" to (duracionAcumulada+duracionMillis),
                        "ultimaSeccionVisitada.nombreSeccion" to nombreSeccion,
                        "ultimaSeccionVisitada.fechaVisita" to Timestamp(Date())
                    ))

                    Log.i(TAG,"Seccion Info updated")
                }
            }
    }

    //Obtiene el porcentaje de visita de un usuario
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun updatePorcentajeVisita(nombreUsuario: String): Int {
        var docRef = FirebaseFirestore.getInstance().collection(USUARIOS).document(nombreUsuario)
        docRef.get()
            .addOnSuccessListener {document ->
                if (document != null)
                {
                    var seccionesVisitadas:Int = 0
                    for(nombreSeccion in ARRAY_NOMBRES_SECCIONES)
                    {
                        if ((document.get("secciones.${nombreSeccion}.visitada") as Boolean))
                        {
                            seccionesVisitadas++
                        }
                    }

                    val porcentaje = ((seccionesVisitadas.toDouble()/ CANTIDAD_SECCIONES) * 100).toLong()

                    // Agregar Porcentaje de Visita a Shared Prefences
                    val preferencesEditor: SharedPreferences.Editor = sp.edit()
                    preferencesEditor.putInt(PORCENTAJE_VISITA, porcentaje.toInt())
                    preferencesEditor.commit()

                    docRef.update(mapOf("stadistics.porcentajeVisita.valor" to porcentaje))

                    Log.i(TAG,porcentajeVisita.toInt().toString())
                    Log.i(TAG,"Seccion Info updated")

                }
            }
        return porcentajeVisita.toInt()
    }

    fun setPorcentajeVisita(p: Double)
    {
        porcentajeVisita = p.toInt()
    }


    //Actualiza los datos de Estadisticas Globales
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun updateEstadisticasGlobales(nombreUsuario: String) {

        var duracion_total = 0
        var hashMap_secciones_visitas = HashMap<String,Int>()
        var list_secciones_visitadas = mutableListOf<String>()
        var list_secciones_no_visitadas = mutableListOf<String>()

        var docRef = db.collection(USUARIOS).document(nombreUsuario)
        var document = db.collection(USUARIOS).document(nombreUsuario)

        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {

                    for(nombreSeccion in ARRAY_NOMBRES_SECCIONES)
                    {
                        // Obtiene la duración acumulada de la sección
                        duracion_total += (document.get("secciones.${nombreSeccion}.duracionAcumuladaMillis") as Long).toInt()

                        // Asigna al HashMap el total de visitas a la sección
                        hashMap_secciones_visitas[nombreSeccion] = (document.get("secciones.${nombreSeccion}.cantidadVecesVisitada") as Long).toInt()

                        // Verifica si la sección ya fue visitada
                        if ((document.get("secciones.${nombreSeccion}.visitada") as Boolean))
                        {
                            list_secciones_visitadas.add(nombreSeccion)
                        }

                        else
                        {
                            list_secciones_no_visitadas.add(nombreSeccion)
                        }

                    }

                    // Se convierten todos los objetos a Json y luego a String, y se proceden a añadir a Shared Prefences
                    val preferencesEditor: SharedPreferences.Editor = sp.edit()
                    preferencesEditor.putInt(DURACION_TOTAL, duracion_total)

                    val jsonHashMap = GsonBuilder().create().toJson(hashMap_secciones_visitas)
                    preferencesEditor.putString(HASHMAP_SECCIONESVISITADAS, jsonHashMap)

                    val jsonList_sv = GsonBuilder().create().toJson(list_secciones_visitadas)
                    preferencesEditor.putString(SECCIONES_VISITADAS, jsonList_sv)

                    val jsonList_snv = GsonBuilder().create().toJson(list_secciones_no_visitadas)
                    preferencesEditor.putString(SECCIONES_POR_VISITAR, jsonList_snv)

                    preferencesEditor.commit()

                    Log.i("duracion_total",duracion_total.toString())
                    Log.i("seccionesVisitas",hashMap_secciones_visitas.size.toString())
                    Log.i("secciones_visitadas",list_secciones_visitadas.size.toString())
                    Log.i("secciones_no_visitadas",list_secciones_no_visitadas.size.toString())

                }



            }

    }

    //Obtiene todos los últimas visitas de todos los usuarios y los persiste en SharedPreferences
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getAllUsuariosLog() {

        var list_usuariosLog = mutableListOf<List<String>>()

        FirebaseFirestore.getInstance().collection(USUARIOS)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    var listaUsuarioLogs: MutableList<String> = mutableListOf<String>()

                    val nombreUsuario = document.get("nombre") as String
                    val nombreUltimaSeccion =document.get("${ULTIMA_SECCION_VISITADA}.nombreSeccion") as String
                    val dateVisita = (document.get("${ULTIMA_SECCION_VISITADA}.fechaVisita")) as Date
                    val dateStringVisita = dateVisita.toString()

                    val diferencia = Calendar.getInstance().time.time - dateVisita.time
                    var elapsedMinutes = diferencia/60000
                    var elapsedSeconds = diferencia/1000


                    var msgTiempo = "hace ${elapsedMinutes.toString()}m ${elapsedSeconds.toString()}s "

                    listaUsuarioLogs.add(nombreUsuario)
                    listaUsuarioLogs.add(nombreUltimaSeccion)
                    listaUsuarioLogs.add(msgTiempo)

                    list_usuariosLog.add(listaUsuarioLogs.toList())
                }

                // Se convierten todos los objetos a Json y luego a String, y se proceden a añadir a Shared Prefences
                val preferencesEditor: SharedPreferences.Editor = sp.edit()

                val jsonList_usuariosLog = GsonBuilder().create().toJson(list_usuariosLog)
                preferencesEditor.putString(ULTIMA_SECCION_VISITADA, jsonList_usuariosLog)
                preferencesEditor.commit()

            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "Error getting documents: ", exception)
            }
    }


}


