package com.example.laluisangelv1.Recorridos

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.Adapters.SeccionListAdapter
import com.example.laluisangelv1.Secciones.RecorridoViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import androidx.lifecycle.Observer
import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ModoRecorridoActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var db: FirebaseFirestore
    lateinit var query: Query

    @BindView(R.id.UAMessage2)
    @JvmField
    var seccionActual: TextView? = null

    @BindView(R.id.seccionesCercanas_rc)
    @JvmField
    var recyclerView: RecyclerView? = null

    @BindView(R.id.linearLayoutNoConnected)
    @JvmField
    var mssgNoConnected: LinearLayout? = null

    private var listaSeccionesCercanas: MutableList<String> = mutableListOf<String>()
    private lateinit var recorridoViewModel: RecorridoViewModel

//    var idRecorrido: Int = Math.random().toInt()
    var idRecorrido: Int = 0
    private var duracionAcumulada: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modo_recorrido)

        //ButterKnife
        ButterKnife.bind(this)

        val b = intent.extras
        if (b != null)
        {
            seccionActual!!.text = b.getString("seccionActual").toString()
            duracionAcumulada = b.getInt("duracionAcumulada")

            var idRecorridoTemp = b.getInt("idRecorrido")

            if (idRecorridoTemp != null )  idRecorrido = idRecorridoTemp
        }

        // Se inicia la corutina para obtener de Firebase la lista de secciones mas cercanas

            getListaSeccionesCercanas()


    }
        override fun onClick(v: View)
    {
        val intent = Intent(this,InstruccionesActivity::class.java)
        startActivity(intent)
    }

    // Obtiene la lista de Secciones Cercanas de Firebase
    private fun getListaSeccionesCercanas()
    {
        logThread("getListaSeccionesCercanas()")


        var adapter = SeccionListAdapter(this)

        val cm = this@ModoRecorridoActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if(isConnected) {
            Log.i("Conectividad","EstaConectado")
            adapter.setConectividad(true)
            db = FirebaseFirestore.getInstance()
            mssgNoConnected!!.setVisibility(View.GONE);

            // [START get_multiple_all]
            db.collection("secciones").document("${seccionActual!!.text}")
                .collection("seccionesCercanas").get()
                .addOnSuccessListener { result ->
                    listaSeccionesCercanas = mutableListOf<String>()
                    for (document in result) {
                        listaSeccionesCercanas.add(document.id)
                    }
                    adapter.setSecciones(listaSeccionesCercanas.toList())
                    adapter.setDuracionAcumulada(duracionAcumulada)

                    adapter.setConectividad(true)
                    this.seccionActual?.let { adapter.setSeccionActual(it!!.text.toString()) }
                }
                .addOnFailureListener { exception ->
                    Log.d("ModoRecorridoActivity", "Error getting documents: ", exception)
                }
            // [END get_multiple_all]

            adapter.setIdRecorrido(this.idRecorrido)
            recyclerView!!.adapter = adapter
            recyclerView!!.layoutManager = LinearLayoutManager(this)

        }
        else{
            mssgNoConnected!!.setVisibility(View.VISIBLE);

            adapter.setConectividad(false)
            // Get a new or existing ViewModel from the ViewModelProvider.
            recorridoViewModel = ViewModelProvider(this).get(RecorridoViewModel::class.java)
            //       recorridoViewModel.deleteAll()
            recorridoViewModel.setidRecorrido(idRecorrido)

            // Add an observer on the LiveData returned by getAlphabetizedWords.
            // The onChanged() method fires when the observed data changes and the activity is
            // in the foreground.
            recorridoViewModel.totalSecciones.observe(this, Observer { sitios ->
                // Update the cached copy of the words in the adapter.
                adapter.setSeccionesClass(sitios)
            })

            setListaSeccionesCercanas(adapter,this)
        }

    }

    // Asigna al RecyclerView la lista de secciones cercanas
    private  fun setListaSeccionesCercanas(adapter: SeccionListAdapter, context: Context)
    {

            recyclerView!!.adapter = adapter
            recyclerView!!.layoutManager = LinearLayoutManager(context)

    }

    private fun logThread(methodName: String){
        Log.i("LogThread","debug: ${methodName}: ${Thread.currentThread().name}")
    }



    internal fun setSeccionActual (seccion: String){
        this.seccionActual!!.text = seccion
    }


}
