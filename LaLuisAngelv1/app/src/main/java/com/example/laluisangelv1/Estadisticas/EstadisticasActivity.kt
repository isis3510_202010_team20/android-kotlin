package com.example.laluisangelv1.Estadisticas

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.Adapters.SeccionesEstadisticasListAdapter
import com.example.laluisangelv1.Recorridos.Model.Firebase.FirebaseRepository
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder


class EstadisticasActivity : AppCompatActivity() {

    private lateinit var fb_repository: FirebaseRepository
    private lateinit var auth: FirebaseAuth
    private lateinit var sp: SharedPreferences

    var ULTIMA_CONEXION: String = "ultimaConexion"

    private var duracionTotal: Int = 0
    private val elapsedMinutes: Int = 60000
    private val elapsedSeconds: Int = 1000
    private lateinit var secciones_visitas: HashMap <String, Int>
    private lateinit var secciones_visitadas: List<String>
    private lateinit var secciones_no_visitadas: List<String>
    var estaConectado: Boolean = true

    //ButterKnife
    @BindView(R.id.usernameEstadisiticasTextView)
    @JvmField
    var usernameEstadisiticasTextView: TextView? = null
    @BindView(R.id.sinconexionTitle_estadisticas)
    @JvmField
    var sinconexionTitleEstadisticas: TextView? = null
    @BindView(R.id.cloudoff_icon_estadisticas)
    @JvmField
    var cloudoff_icon_estadisticas: ImageView? = null
    @BindView(R.id.barchart)
    @JvmField
    var barchart: BarChart? = null
    @BindView(R.id.duracionAcumuladaText)
    @JvmField
    var duracionAcumuladaText: TextView? = null
    @BindView(R.id.seccionesVisitadasRecyclerView)
    @JvmField
    var seccionesVisitadasRecyclerView: RecyclerView? = null
    @BindView(R.id.seccionesporvisitarRecyclerView)
    @JvmField
    var seccionesporvisitarRecyclerView: RecyclerView? = null

    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estadisticas)

        ButterKnife.bind(this)

        // Init Shared Preferences
        sp = PreferenceManager.getDefaultSharedPreferences(this)

        verificarConexion()

        // Init FirebaseAuth
        auth = FirebaseAuth.getInstance()

        // Init Firebase y se actualizan las estadistícas globales
        fb_repository = FirebaseRepository(this)
        fb_repository.updateEstadisticasGlobales(auth.currentUser?.email.toString())

        // Init objetos de Estadisticas
        secciones_visitas = hashMapOf<String,Int>()
        secciones_visitadas = mutableListOf<String>()
        secciones_no_visitadas = mutableListOf<String>()


        //Se obtienen los datos de Estadisticas de SharedPreferences
        getObjetosEstadisticas()

        //Set username
        usernameEstadisiticasTextView?.setText(auth.currentUser?.email.toString())

        // Set Bar Chart
        setBarChart()

        // Set Tiempo total del visita
        setTiempoTotalVisita()

        //Set Lista Secciones
        setRecyclerViewSecciones()

    }

    private fun verificarConexion()
    {
        val cm = this@EstadisticasActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        estaConectado = activeNetwork?.isConnectedOrConnecting == true

        if(estaConectado)
        {
            sinconexionTitleEstadisticas?.visibility  = View.GONE
            cloudoff_icon_estadisticas?.visibility = View.GONE
        }

        else
        {
            sinconexionTitleEstadisticas?.visibility  = View.VISIBLE
            cloudoff_icon_estadisticas?.visibility= View.VISIBLE
            val ultimaConexion = sp.getString(ULTIMA_CONEXION,"")

            val stringUltimaConexión = "Sin conexión. (Datos de ${ultimaConexion}"
            sinconexionTitleEstadisticas?.setText(stringUltimaConexión)
        }
    }

    // Se asignan los valores a cada barra del BarChart, en este caso con las secciones y su cantidad de visitas
    private fun setBarChart() {
        var barChart = barchart
        val entries = ArrayList<BarEntry>()
        val labels = ArrayList<String>()

        var counter = 0

        for ( (seccion,visitas) in secciones_visitas )
        {
          entries.add(BarEntry(visitas.toFloat(),counter))
          labels.add(seccion)
          counter++

        }

        val barDataSet = BarDataSet(entries, "Secciones")


        val data = BarData(labels, barDataSet)
        if (barChart != null) {
            barChart.data = data
        } // set the data and list of lables into chart

        if (barChart != null) {
            barChart.setDescription("Visitas por Sección")
        }  // set the description

        //barDataSet.setColors(ColorTemplate.COLORFUL_COLORS)
        barDataSet.color = resources.getColor(R.color.colorBar)

        if (barChart != null) {
            barChart.animateY(2500)
        }
    }

    // Obtiene la duración total acumulada en milisegundos y los convierte en minutos y segundos
    private fun setTiempoTotalVisita () {


        duracionAcumuladaText?.setText("${(duracionTotal/elapsedMinutes).toString()}m ${((duracionTotal%elapsedMinutes)/elapsedSeconds).toString()}s")
    }

    private fun setRecyclerViewSecciones() {
        val recyclerView_visitadas = seccionesVisitadasRecyclerView
        val recyclerView_porvisitar = seccionesporvisitarRecyclerView

        val adapter_visitadas = SeccionesEstadisticasListAdapter(this)
        val adapter_porvisitar = SeccionesEstadisticasListAdapter(this)

        adapter_visitadas.setSecciones(secciones_visitadas)
        adapter_porvisitar.setSecciones(secciones_no_visitadas)

        recyclerView_visitadas?.adapter = adapter_visitadas
        recyclerView_porvisitar?.adapter = adapter_porvisitar

        recyclerView_visitadas?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView_porvisitar?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun getObjetosEstadisticas()
    {

        val hashMapString = sp.getString(FirebaseRepository.HASHMAP_SECCIONESVISITADAS, hashMapOf<String,Int>().toString())
        secciones_visitas = GsonBuilder().create().fromJson(hashMapString,secciones_visitas.javaClass)

        val seccionesVisitadasString = sp.getString(FirebaseRepository.SECCIONES_VISITADAS, emptyList<String>().toString())
        secciones_visitadas =  GsonBuilder().create().fromJson(seccionesVisitadasString,secciones_visitadas.javaClass)

        val seccionesNoVisitadasString = sp.getString(FirebaseRepository.SECCIONES_POR_VISITAR, emptyList<String>().toString())
        secciones_no_visitadas =  GsonBuilder().create().fromJson(seccionesNoVisitadasString,secciones_no_visitadas.javaClass)

        duracionTotal = sp.getInt(FirebaseRepository.DURACION_TOTAL,0)

    }
}
