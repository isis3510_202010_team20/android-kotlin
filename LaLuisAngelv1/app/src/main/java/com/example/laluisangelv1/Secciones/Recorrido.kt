package com.example.laluisangelv1.Secciones

import androidx.room.*


@Entity(tableName = "recorrido_table")
data class Recorrido(@PrimaryKey (autoGenerate = true) @ColumnInfo (name = "idRecorrido") val idRecorrido: Int,
                     @ColumnInfo(name = "fechaInicio") var fechaInicio: String,
                     @ColumnInfo(name = "fechaFinal") var fechaFinal: String,
                     @ColumnInfo(name = "duracionTotalMins") var duracionTotalMins: Int)

@Entity(tableName = "seccion_table")
data class Seccion(@PrimaryKey @ColumnInfo (name = "nombreseccion") val nombreseccion: String,
                     @ColumnInfo(name = "idRecorrido") val idRecorrido: Int,
                     @ColumnInfo(name = "descripcion") var descripcion: String,
                    @ColumnInfo(name = "duracionMins") var duracionMins: Int)

@Entity(tableName = "parqueaderos")
data class Parqueaderos(@PrimaryKey @ColumnInfo (name = "codigo") val nombre: String,
                   @ColumnInfo(name = "ocupado") val ocupado: Boolean)

@Entity(tableName = "comentarios")
data class Comentarios(@PrimaryKey @ColumnInfo (name = "user") val user: String,
                        @ColumnInfo(name = "comentario") val comentario: String,
                       @ColumnInfo(name = "valoracion") val valoracion: String)

