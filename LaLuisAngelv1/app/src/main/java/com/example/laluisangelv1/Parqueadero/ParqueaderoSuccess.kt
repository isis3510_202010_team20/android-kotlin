package com.example.laluisangelv1.Parqueadero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.laluisangelv1.Home.HomeActivity
import com.example.laluisangelv1.R
import kotlinx.android.synthetic.main.activity_parqueadero_success.*

class ParqueaderoSuccess : AppCompatActivity(),View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parqueadero_success)

        homeReturn.setOnClickListener(this)
    }
    override fun onClick(v: View) {
        val i = v.id
        when (i) {
            R.id.homeReturn ->returnHome()
        }
    }
    private fun returnHome() {
        val intent = Intent(this,HomeActivity::class.java)
        startActivity(intent)
    }


}

