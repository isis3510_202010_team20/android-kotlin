package com.example.laluisangelv1.Recorridos.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.Home.HomeActivity
import com.example.laluisangelv1.R

class UsuariosListAdapter internal constructor(activity: HomeActivity) : RecyclerView.Adapter<UsuariosListAdapter.UsuariosViewHolder>()

{

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var conected:Boolean = true;
    var nombresUsuarios: List<String> = emptyList()
    var nombresSecciones:List<String> =  emptyList()
    var fechasVisita: List<String> = emptyList()
    var total: MutableList<List<String>> = mutableListOf<List<String>>()

    inner class UsuariosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val usuarioItemView: ConstraintLayout = itemView.findViewById(R.id.usuariosLog_rv_cl)
        val nombreUsuario: TextView = usuarioItemView.findViewById(R.id.username_logTextView)
        val nombreSeccion: TextView = usuarioItemView.findViewById(R.id.seccion_logTextView)
        val fechaVisita: TextView = usuarioItemView.findViewById(R.id.tiempo_logTextView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsuariosViewHolder {
        val itemView = inflater.inflate(R.layout.usuarioslog_rv_item, parent, false)

        return UsuariosViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UsuariosViewHolder, position: Int) {

        holder.nombreUsuario.text = total[position][0]
        holder.nombreSeccion.text = total[position][1]
        holder.fechaVisita.text = total[position][2]
    }

    internal fun setNombresUsuarios(nombresUsuarios:List<String>) {

        this.nombresUsuarios = nombresUsuarios
        notifyDataSetChanged()
    }
    internal fun setNombresSecciones(nombresSecciones:List<String>) {
        this.nombresSecciones = nombresSecciones
        notifyDataSetChanged()
    }
    internal fun setFechasVisita(fechasVisita:List<String>) {
        this.fechasVisita = fechasVisita
        notifyDataSetChanged()
    }
    internal fun setTotal(usuariosLogs: List<String>) {
        this.total.add(usuariosLogs)
        notifyDataSetChanged()
    }
    internal fun setTotalFull(usuariosLogs: MutableList<List<String>>) {
        this.total = usuariosLogs
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return this.total.size
    }

}