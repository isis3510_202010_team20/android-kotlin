package com.example.laluisangelv1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.viewModelScope
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.Recorridos.Model.Firebase.FirebaseRepository
import com.example.laluisangelv1.Secciones.Seccion
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignUpActivity : AppCompatActivity(), View.OnClickListener {

    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]
    private lateinit var fb_repository: FirebaseRepository

    @BindView(R.id.correoSignUpText)
    @JvmField
    var emailView: EditText? = null

    @BindView(R.id.contraseñaSignUpText)
    @JvmField
    var passwordView: EditText? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        //ButterKnife
        ButterKnife.bind(this)

        // Buttons
        signUpButton.setOnClickListener(this)

        // [START initialize_auth]
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        // [END initialize_auth]
        fb_repository = FirebaseRepository(this)


    }

    private fun createAccount() {

        Log.d("SignUpActivity", "createAccount:$emailView.text.toString().trim()")
        if (!validateForm()) {
            return
        }

        // [START create_user_with_email]
        auth.createUserWithEmailAndPassword(emailView!!.text.toString().trim(), passwordView!!.text.toString().trim())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("SignUpActivity", "createUserWithEmail:success")
                    val user = auth.currentUser

                    fb_repository.crearUsuario(emailView!!.text.toString().trim())

                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("SignUpActivity", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

            }
        // [END create_user_with_email]
    }

    override fun onClick(v: View) {
        val i = v.id
        when (i) {
            R.id.signUpButton -> createAccount()

        }
    }



    private fun validateForm(): Boolean {
        var valid = true

        if (TextUtils.isEmpty(emailView!!.text)) {
            emailView!!.error = "Required."
            valid = false
        } else {
            emailView!!.error = null
        }

        val password = passwordView!!.text.toString()
        if (TextUtils.isEmpty(passwordView!!.text)) {
            passwordView!!.error = "Required."
            valid = false
        } else {
            passwordView!!.error = null
        }

        return valid
    }

}
