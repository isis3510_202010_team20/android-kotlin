package com.example.laluisangelv1.Parqueadero

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.Parqueadero.Adapters.ParqueaderoListAdapter
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Secciones.Parqueaderos
import com.example.laluisangelv1.Secciones.RecorridoViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Parqueadero : AppCompatActivity() {

    lateinit var db: FirebaseFirestore
    private var listaParqueadero: MutableList<ParqueaderoData> = mutableListOf<ParqueaderoData>()

    @BindView(R.id.parqueaderos)
    @JvmField
    var recyclerView:RecyclerView? = null

    private lateinit var recorridoViewModel: RecorridoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parqueadero)

        //ButterKnife
        ButterKnife.bind(this)

        val cm = this@Parqueadero.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        val adapter = ParqueaderoListAdapter(this)

        adapter.setConectividad(false)
        // Get a new or existing ViewModel from the ViewModelProvider.
        recorridoViewModel = ViewModelProvider(this).get(RecorridoViewModel::class.java)


        if(isConnected) {
            db = FirebaseFirestore.getInstance()

            // [START get_multiple_all]
            var docRef = db.collection("parqueadero").get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        listaParqueadero = mutableListOf<ParqueaderoData>()
                        document.documents.forEach()
                        {

                            val parqueaderoDatos:ParqueaderoData = ParqueaderoData(it.data?.get("codigo") as String,it.data?.get("ocupado") as Boolean)
                            listaParqueadero.add(parqueaderoDatos)
                            val parqueaderos:Parqueaderos = Parqueaderos(parqueaderoDatos.name ,parqueaderoDatos.ocupado )
                            recorridoViewModel.insertParqueadero(parqueaderos)

                        }
                        adapter.setParqueaderos(listaParqueadero)

                    } else {
                        Log.d("SeccionActivity", "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("InstruccionesActivity", "get failed with ", exception)
                }
            recyclerView!!.adapter = adapter
            recyclerView!!.layoutManager = LinearLayoutManager(this)

        }
        else
        {


            // The onChanged() method fires when the observed data changes and the activity is
            // in the foreground.
            recorridoViewModel.totalParqueaderoReservado.observe(this, Observer { parqueaderos ->
                // Update the cached copy of the words in the adapter.
                adapter.setParqueaderosLocal(parqueaderos)
            })
            recyclerView!!.adapter = adapter
            recyclerView!!.layoutManager = LinearLayoutManager(this)


        }
    }
}
