package com.example.laluisangelv1.Recorridos.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.SeccionActivity

class ComentariosListAdapter internal constructor(activity: SeccionActivity) : RecyclerView.Adapter<ComentariosListAdapter.ComentariosViewHolder>()

{

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var conected:Boolean = true;
    var usuario: List<String> = emptyList()
    var comentario:List<String> =  emptyList()
    var valoracion: List<String> = emptyList()
    var total: MutableList<List<String>> = mutableListOf<List<String>>()

    inner class ComentariosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val comentarioItemView: ConstraintLayout = itemView.findViewById(R.id.comentario_rv)
        val usuario: TextView = comentarioItemView.findViewById(R.id.username_logTextView)
        val comentario: TextView = comentarioItemView.findViewById(R.id.comentarioTextView)
        val star1: ImageView = comentarioItemView.findViewById(R.id.imageView3)

        val star2: ImageView = comentarioItemView.findViewById(R.id.imageView4)

        val star3: ImageView = comentarioItemView.findViewById(R.id.imageView5)

        val star4: ImageView = comentarioItemView.findViewById(R.id.imageView12)

        val star5: ImageView = comentarioItemView.findViewById(R.id.imageView10)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComentariosViewHolder {
        val itemView = inflater.inflate(R.layout.comentario_rv_item, parent, false)

        return ComentariosViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ComentariosViewHolder, position: Int) {

        holder.usuario.text = total[position][0]
        holder.comentario.text = total[position][1]

        val p = total[position][2].toInt()
        when (p) {
            1 -> {
                holder.star1.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star2.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star3.setImageResource(R.drawable.ic_star_border_black_24dp)
                holder.star4.setImageResource(R.drawable.ic_star_border_black_24dp)
                holder.star5.setImageResource(R.drawable.ic_star_border_black_24dp)
            }
            2 -> {
                holder.star1.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star2.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star3.setImageResource(R.drawable.ic_star_border_black_24dp)
                holder.star4.setImageResource(R.drawable.ic_star_border_black_24dp)
                holder.star5.setImageResource(R.drawable.ic_star_border_black_24dp)
            }
            3 -> {
                holder.star1.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star2.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star3.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star4.setImageResource(R.drawable.ic_star_border_black_24dp)
                holder.star5.setImageResource(R.drawable.ic_star_border_black_24dp)
            }
            4 -> {
                holder.star1.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star2.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star3.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star4.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star5.setImageResource(R.drawable.ic_star_border_black_24dp)
            }
            5 -> {
                holder.star1.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star2.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star3.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star4.setImageResource(R.drawable.ic_star_black_24dp)
                holder.star5.setImageResource(R.drawable.ic_star_black_24dp)
            }
            else -> {

            }
        }

    }

    internal fun setUsuarios(usuario:List<String>) {

        this.usuario = usuario
        notifyDataSetChanged()
    }
    internal fun setComentarios(comentarios:List<String>) {
        this.comentario = comentarios
        notifyDataSetChanged()
    }
    internal fun setValoraciones(valoracion:List<String>) {
        this.valoracion = valoracion
        notifyDataSetChanged()
    }
    internal fun setTotal(secciones: List<String>) {
        this.total.add(secciones)
        notifyDataSetChanged()
    }
    internal fun setTotalFull(secciones: MutableList<List<String>>) {
    this.total = secciones
    notifyDataSetChanged()
}


    override fun getItemCount(): Int {
        return this.total.size
    }

}
