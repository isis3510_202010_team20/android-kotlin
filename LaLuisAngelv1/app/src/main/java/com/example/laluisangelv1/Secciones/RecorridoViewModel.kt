package com.example.laluisangelv1.Secciones

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.laluisangelv1.Parqueadero.Parqueadero
import com.example.laluisangelv1.Parqueadero.ParqueaderoData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View Model to keep a reference to the recorrido repository and
 * an up-to-date list of all recorrido' secciones.
 */

class RecorridoViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: RecorridoRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val totalSecciones: LiveData<List<Seccion>>
    val totalParqueaderoReservado: LiveData<List<Parqueaderos>>
    val totalComentarios: LiveData<List<Comentarios>>
    var idRecorrido: Int = 0

    init {


        val recorridoDao = RecorridoRoomDatabase.getDatabase(application, viewModelScope).recorridoDao()
        repository = RecorridoRepository(recorridoDao)
        totalSecciones = repository.totalSecciones
        totalParqueaderoReservado = repository.totalParqueaderoReservado
        totalComentarios = repository.totalComentarios
    }

    fun setidRecorrido(idRecorrido: Int)
    {
        this.idRecorrido = idRecorrido
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insertRecorrido(recorrido: Recorrido) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertRecorrido(recorrido)
    }

    fun insertSeccion(seccion: Seccion) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertSeccion(seccion)
    }
    fun insertParqueadero(parqueadero: Parqueaderos) = viewModelScope.launch(Dispatchers.IO) {

        repository.insertParqueadero(parqueadero)
    }
    fun insertComentario(comentarios: Comentarios) = viewModelScope.launch(Dispatchers.IO) {

        repository.insertComentario(comentarios)
    }

    fun deleteAll () = viewModelScope.launch(Dispatchers.IO)
    {
        repository.deleteAll()
    }
}
