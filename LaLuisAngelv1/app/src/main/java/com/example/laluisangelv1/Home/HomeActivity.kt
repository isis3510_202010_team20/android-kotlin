package com.example.laluisangelv1.Home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.laluisangelv1.EXTRA_MESSAGE
import com.example.laluisangelv1.Estadisticas.EstadisticasActivity
import com.example.laluisangelv1.LoginActivity
import com.example.laluisangelv1.Parqueadero.Parqueadero
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.Adapters.UsuariosListAdapter
import com.example.laluisangelv1.Recorridos.Model.Firebase.FirebaseRepository
import com.example.laluisangelv1.Recorridos.Model.PorcentajeViewModel
import com.example.laluisangelv1.Recorridos.ModoRecorridoActivity
import com.example.laluisangelv1.Secciones.RecorridoListAdapter
import com.example.laluisangelv1.Secciones.RecorridoViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_home.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

import com.newrelic.agent.android.NewRelic;
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class HomeActivity : AppCompatActivity(), View.OnClickListener {

    // Se crean las instancias que manejan la persistencia y auth, en este caso Firebase y Shared Preferences

    var context: HomeActivity = this

    private lateinit var auth: FirebaseAuth
    private lateinit var fb_repository: FirebaseRepository
    private val sharedPrefFile: String = "com.example.laluisangelv1"
    private lateinit var sp: SharedPreferences
    var estaConectado: Boolean = true

    // Se inicializan las instancias de HomeActivity y del Layout
    var seccionActual : String = "Entrada"
    var ULTIMA_CONEXION: String = "ultimaConexion"
    private val PORCENTAJE_VISITA = "porcentajeVisita"
    private val comentarioNuevoActivityRequestCode = 1
    private lateinit var recorridoViewModel: RecorridoViewModel
    private var idRecorrido: Int = 0
    private lateinit var porcentajeViewModel : PorcentajeViewModel

    //Instancias del Layout
    //ButterKnife

    @BindView(R.id.sinconexionTitle)
    @JvmField
    var sinConexionTitle: TextView? = null
    @BindView(R.id.cloudoff_icon)
    @JvmField
    var cloudOffIcon: ImageView? = null
    @BindView(R.id.usernameHomeActivity)
    @JvmField
    var userNameTitle: TextView? = null
    @BindView(R.id.SeccionActualHome)
    @JvmField
    var seccionActualTitle: TextView? = null
    @BindView(R.id.progressBarRecorrido)
    @JvmField
    var progressBarRecorrido: ProgressBar? = null
    @BindView(R.id.porcentajeExploradoView)
    @JvmField
    var progressBarPorcentajeTitle: TextView? = null
    @BindView(R.id.recyclerview_history)
    @JvmField
    var recyclerViewVisitas: RecyclerView? = null
    @BindView(R.id.usuariosLog_rv)
    @JvmField
    var recyclerViewUsuarios: RecyclerView? = null


    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //ButterKnife
        ButterKnife.bind(this)

            initInstanciasLayout()


        // Se inicializan todas las instancias de persistencia y Auth (Firestore, SharedPreferences y FirebaseAuth)

            initPersistanceInstances()


        //Se verifica si hay conexión

            verificarConexion()


        //Se inicializan los buttons de HomeActivity

            initButtons()



     //   Thread(Runnable {
            // Se establece el porcentaje de visita del usuario (Corutina)
            CoroutineScope(IO).launch {
                initPorcentajeVisita()
            }
     //   }).start()

        //Se obtienen los extras almacenados en el Intent

            getIntentExtras()


        //Init Vista de Ultimas Visitas (Corutina)

     //   Thread(Runnable {
            CoroutineScope(IO).launch {
                initUltimasVisitas()
            }
     //   }).start()

     //   Thread(Runnable {
            // Se inicializa las últimas visitas de los demas usuarios (Corutina)
            CoroutineScope(IO).launch {
                initUsuariosLog()
            }
    //    }).start()

        // Se inicializan las estadistícas de New Relic

            initNewRelic()

    }

    private  fun initNewRelic()
    {
        NewRelic.withApplicationToken(
                "AA067eedce69ec923c8b720269b7d88491ef6de2df-NRMA"
        ).start(this.getApplication());
    }

    public override fun onNewIntent(intent: Intent)
    {
        super.onNewIntent(intent)
        updateHomeUI(intent)
    }

    public override fun onRestart() {
        super.onRestart()
        Log.i("HomeActivity", "onRestart()")
        // Check if user is signed in (non-null) and update UI accordingly.
    }

    public override fun onStart() {
        super.onStart()
        Log.i("HomeActivity", "onStart()")
        // Check if user is signed in (non-null) and update UI accordingly.
    }

    public override fun onResume() {
        super.onResume()
        Log.i("HomeActivity", "onResume()")
//        updateHomeUI()
    }

    public override fun onPause()
    {
        super.onPause()
        Log.i("HomeActivity", "onPause()")
    }

    public override fun onStop() {
        super.onStop()
        Log.i("HomeActivity", "onStop()")
        if(estaConectado) {
            updateFechaUltimaVisita()
        }
    }

    public override fun onDestroy()
    {
        super.onDestroy()
        Log.i("HomeActivity", "onDestroy()")
    }

    private fun initInstanciasLayout()
    {
        // Se inicializa el ViewModel del Porcentaje de visita

            porcentajeViewModel = PorcentajeViewModel()


    }


     fun verificarConexion()
    {
        val a = this



        val cm = this@HomeActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        estaConectado = activeNetwork?.isConnectedOrConnecting == true

        if(estaConectado)
        {
            sinConexionTitle!!.visibility = View.GONE
            cloudOffIcon!!.visibility = View.GONE
        }

        else
        {

            val builder = AlertDialog.Builder(a)

            // Set the alert dialog title
            builder.setTitle("Sin conexión")

            // Display a message on alert dialog
            builder.setMessage("Parece que no estas conectado a Internet. Mostraremos los datos de tu última conexión")

            // Set a positive button and its click listener on alert dialog
            builder.setPositiveButton("ENTENDIDO"){dialog,which -> }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()

            sinConexionTitle!!.visibility = View.VISIBLE
            cloudOffIcon!!.visibility = View.VISIBLE

            val ultimaConexion = sp.getString(ULTIMA_CONEXION,"")

            val stringUltimaConexión = "Sin conexión. (Datos de ${ultimaConexion})"
            sinConexionTitle!!.text= stringUltimaConexión
        }

    }
    private fun initPersistanceInstances()
    {
        val a = this

            sp = PreferenceManager.getDefaultSharedPreferences(a)
            fb_repository = FirebaseRepository(a)
            auth = FirebaseAuth.getInstance()

            // Se establece el nombre de usuario en el Layout
            userNameTitle!!.text = "Hola ${auth.currentUser?.email}"



    }

    private  fun initButtons()
    {
        val a =this

            // Inicializa los Listeners de los botones de la Actividad
            modoRecorridoButton.setOnClickListener(a)
            logOutButton.setOnClickListener(a)
            button.setOnClickListener(a)
            parqueaderoButton.setOnClickListener(a)
            locationLogo.setImageResource(R.drawable.ic_location_on_black_24dp);

    }

    private fun getIntentExtras()
    {

            val extras = intent.extras

            // Se obtienen los extras del Intent que inicio la actividad Home
            if (extras != null) {
                seccionActual= intent.getStringExtra("seccionActual")
                //     idRecorrido =  extras.getInt("idRecorrido")
                seccionActualTitle!!.text = seccionActual
            }

            userNameTitle!!.text = "Hola ${auth.currentUser?.email}"

            // Get the Intent that started this activity and extract the string
            val message = intent.getStringExtra(EXTRA_MESSAGE)


    }

    private suspend fun initPorcentajeVisita()
    {
        withContext(Main)
        {
            if (estaConectado) {
                // Se establece el porcentaje de visita del usuario
                fb_repository.updatePorcentajeVisita(auth.currentUser?.email.toString())
            }

            val porcentaje = sp.getInt(PORCENTAJE_VISITA, 0)

            val porcentajeObserver = Observer<Int> { paramPorcentaje ->
                // Se actualiza el UI, en este caso el Int del porcentaje
                progressBarRecorrido!!.progress = paramPorcentaje
            }

            val porcentajeTextObserver = Observer<String> { paramPorcentajeText ->
                // Se actualiza el UI, en este caso el Int del porcentaje
                progressBarPorcentajeTitle!!.text = paramPorcentajeText
            }

            // Observar el LiveData del texto y el ProgressBar del Porcentaje de Visita, pasando en esta actividad como el LifecycleOwner y el observador.
            porcentajeViewModel.porcentajeActual.observe(context, porcentajeObserver)
            porcentajeViewModel.porcentajeTextActual.observe(context, porcentajeTextObserver)

            porcentajeViewModel.porcentajeActual.value = porcentaje
            porcentajeViewModel.porcentajeTextActual.value = "${porcentaje.toString()}%"

        }

    }

    private suspend fun initUltimasVisitas()
    {
        withContext(Main) {
            // Obtiene la lista de Ultimas Visitas
            val adapter = RecorridoListAdapter(context)
            recyclerViewVisitas!!.adapter = adapter
            recyclerViewVisitas!!.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            // Get a new or existing ViewModel from the ViewModelProvider.
            recorridoViewModel = ViewModelProvider(context).get(RecorridoViewModel::class.java)
            //       recorridoViewModel.deleteAll()
            recorridoViewModel.setidRecorrido(idRecorrido)
            // Add an observer on the LiveData returned by totalSecciones.
            // The onChanged() method fires when the observed data changes and the activity is
            // in the foreground.
            recorridoViewModel.totalSecciones.observe(context, Observer { sitios ->
                // Update the cached copy of the words in the adapter.
                sitios?.let { adapter.setSecciones(it) }
            })
        }
    }

    private suspend fun initUsuariosLog()
    {
        withContext(Main)
        {
            var usuariosLogList = mutableListOf<List<String>>()

            recyclerViewUsuarios!!.addItemDecoration(
                DividerItemDecoration(
                    recyclerViewUsuarios!!.getContext(),
                    DividerItemDecoration.VERTICAL
                )
            )

            val adapter = UsuariosListAdapter(context)

            if (estaConectado) {
                // Se obtienen y se persisten en SharedPrefences todas las ultimas secciones visitadas de los usuarios
                fb_repository.getAllUsuariosLog()
            }

            val usuariosLogListString = sp.getString(
                FirebaseRepository.ULTIMA_SECCION_VISITADA,
                mutableListOf<List<String>>().toString()
            )
            usuariosLogList =
                GsonBuilder().create().fromJson(usuariosLogListString, usuariosLogList.javaClass)
            var logsList: MutableList<List<String>> = mutableListOf<List<String>>()

            adapter.setTotalFull(usuariosLogList)
            recyclerViewUsuarios!!.adapter = adapter
            recyclerViewUsuarios!!.layoutManager = LinearLayoutManager(context)
        }
    }


    fun updateFechaUltimaVisita()
    {
        var fechaActual = Calendar.getInstance().time

        val fechaDatosString: String = "${fechaActual.toString()}"

        // Agregar String de Fecha de Última Conexión a Shared Prefences
        val preferencesEditor: SharedPreferences.Editor = sp.edit()
        preferencesEditor.putString(ULTIMA_CONEXION, fechaDatosString)
        preferencesEditor.commit()
    }

    private fun updateHomeUI(intent: Intent)
    {
        val extras = intent.extras

        // Se obtienen los extras del Intent que inicio la actividad Home
        if (extras != null) {
            seccionActual= intent.getStringExtra("seccionActual")
            //     idRecorrido =  extras.getInt("idRecorrido")
            seccionActualTitle!!.text = seccionActual
        }

        CoroutineScope(IO).launch {
            verificarConexion()
        }

        CoroutineScope(IO).launch {
            initPorcentajeVisita()
        }

        CoroutineScope(IO).launch {
            initUltimasVisitas()
        }

        CoroutineScope(IO).launch {
            initUsuariosLog()
        }
    }

    override fun onClick(v: View) {
        val i = v.id

        when (i) {
            R.id.modoRecorridoButton -> goModoRecorridoActivity(v)
            R.id.parqueaderoButton -> goParqueaderoActivity()
            R.id.button -> goEstadisticasActivity()
            R.id.logOutButton -> signOut()
        }
    }

    private fun goModoRecorridoActivity(view: View) {
        updateFechaUltimaVisita()
        val intent = Intent(this, ModoRecorridoActivity::class.java)
        intent.putExtra("seccionActual",seccionActual)
        intent.putExtra("idRecorrido",Math.random())
        startActivity(intent)


    }


    private fun goEstadisticasActivity(){
        updateFechaUltimaVisita()
        val intent = Intent(this, EstadisticasActivity::class.java)
        startActivity(intent)
    }

    private fun signOut() {
        updateFechaUltimaVisita()
        auth.signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
    private fun goParqueaderoActivity() {
        updateFechaUltimaVisita()
        val intent = Intent(this, Parqueadero::class.java)
        startActivity(intent)


    }



}
