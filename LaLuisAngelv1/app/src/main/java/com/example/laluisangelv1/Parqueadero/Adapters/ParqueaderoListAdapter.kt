package com.example.laluisangelv1.Parqueadero.Adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.laluisangelv1.Parqueadero.Parqueadero
import com.example.laluisangelv1.Parqueadero.ParqueaderoData
import com.example.laluisangelv1.Parqueadero.ParqueaderoSuccess
import com.example.laluisangelv1.R
import com.example.laluisangelv1.Recorridos.InstruccionesActivity
import com.example.laluisangelv1.Recorridos.SeccionActivity
import com.example.laluisangelv1.Secciones.Parqueaderos
import com.example.laluisangelv1.Secciones.Seccion
import com.google.firebase.firestore.FirebaseFirestore

class ParqueaderoListAdapter internal constructor(activity: Parqueadero) : RecyclerView.Adapter<ParqueaderoListAdapter.ParqueaderoViewHolder>()

{

    lateinit var db: FirebaseFirestore
    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var parqueaderos = emptyList<ParqueaderoData>() // Cached copy of words
    private var conected:Boolean = true
    private val contextAdapter = activity

    inner class ParqueaderoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val sitioItemView: Button = itemView.findViewById(R.id.parqueaderoItem)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParqueaderoViewHolder {
        val itemView = inflater.inflate(R.layout.activity_parqueadero_rv_item, parent, false)

        return ParqueaderoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParqueaderoViewHolder, position: Int) {
        holder.sitioItemView.text = parqueaderos[position].name
        if(!(parqueaderos[position].ocupado))
        {
            holder.sitioItemView.setOnClickListener {
                db = FirebaseFirestore.getInstance()
                        db.collection("parqueadero").document("${parqueaderos[position].name}").update(mapOf(
                                "ocupado" to true
                            ))
                    .addOnFailureListener { exception ->
                        Log.d("InstruccionesActivity", "get failed with ", exception)
                    }

                    val intent = Intent(contextAdapter, ParqueaderoSuccess::class.java)
                    contextAdapter.startActivity(intent)
            }
        }
        else
        {
            holder.sitioItemView.setEnabled(false)
            holder.sitioItemView.setBackgroundColor(Color.RED)
        }

    }

    internal fun setConectividad(conectado: Boolean){
        this.conected = conectado
    }
    internal fun setParqueaderos(parqueaderos: List<ParqueaderoData>) {
        this.parqueaderos = parqueaderos
        notifyDataSetChanged()
    }

    internal fun setParqueaderosLocal(parqueaderos: List<Parqueaderos>) {
        var myList: MutableList<ParqueaderoData> = mutableListOf<ParqueaderoData>()
        parqueaderos.forEach {
            val parqueaderoDatos:ParqueaderoData = ParqueaderoData(it.nombre,it.ocupado)
            myList.add(parqueaderoDatos)
        }
        this.parqueaderos = myList
        notifyDataSetChanged()
    }
    override fun getItemCount() = parqueaderos.size
    private fun publicar()
    {


    }
}
